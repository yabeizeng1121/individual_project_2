# Individual Project 2
## Overview
This project is a simple REST API microservice written in Rust, designed to demonstrate the basic capabilities of serving web requests with Actix-Web framework. The microservice is containerized using Docker, which ensures easy deployment and environment consistency. We have also set up a Continuous Integration and Continuous Deployment (CI/CD) pipeline to automate the testing and deployment processes.

## Features

- Random Greeting Endpoint: Returns a random greeting from a predefined list
- Personalized Motivation Endpoint: Returns a personalized motivation message.

## Preparation
1. In the terminal, write
   ```
   cargo lambda new <your_project_name>
   ```
2. cd to your project folder
3. Go to the `src` directory and modify the `main.rs` file in your own fashion
4. Add html pages in your own fashion
5. After that, use 
   ```
   cargo run
   ```
   to see the local web page, and for mine the local site will be http://localhost:50505
6. Add the dockerfile
7. In your terminal, run
   ```
   docker build -t <your_image_name> .
   ```
8. After that, run the docker container:
   ```
   docker run -d -p 50505:50505 <your_image_name>

   ```
9.  Add the Makefile and the CICD, locally run to see if it works locally
    ```
    make all
    ```

10. create a blank project on `Gitlab` with out a `README.md`
11. Follow the instruction on `Pushing an existing folder` to push your local folder to Gitlab
    
## Demo Video
please find the Demo Video [here](https://youtu.be/_SDzV_JaXN8)
## Results Preview
**Web Page**
![Alt text](img/image.png)
![Alt text](img/image-1.png)
![Alt text](img/image-2.png)

**Docker**
![Alt text](img/image-3.png)