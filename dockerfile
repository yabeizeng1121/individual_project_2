#################
## build stage ##
#################
FROM rust:1-slim-bookworm AS builder
WORKDIR /code

# Download crates-io index and fetch dependency code.
# This step avoids needing to spend time on every build downloading the index
# which can take a long time within the docker context. Docker will cache it.
RUN USER=root cargo init
COPY Cargo.toml Cargo.toml
RUN cargo fetch

# copy app files
COPY src src

# copy static files
COPY static static

# compile app
RUN cargo build --release

###############
## run stage ##
###############
FROM bitnami/minideb:bookworm
WORKDIR /app

# copy server binary from build stage
COPY --from=builder /code/target/release/individual2 individual2

# copy static files
COPY --from=builder /code/static static

# indicate what port the server is running on
EXPOSE 50505

# run server
CMD [ "/app/individual2" ]
