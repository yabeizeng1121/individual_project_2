use actix_files::Files;
use actix_web::{web, App, HttpResponse, HttpServer, Responder};
use rand::Rng;

// Function to return a random greeting as plain text
async fn random_greeting() -> impl Responder {
    let greetings = vec![
        "Hello, world!",
        "Hi there!",
        "Good day!",
        "Bonjour!",
        "Hola!",
        "Guten Tag!",
        "こんにちは (Konnichiwa)!",
        "안녕하세요 (Annyeonghaseyo)!",
        "Привет (Privet)!",
        "Ciao!",
        "你好 (Nihao)",
    ];
    let mut rng = rand::thread_rng();
    let greeting = greetings[rng.gen_range(0..greetings.len())];
    HttpResponse::Ok().content_type("text/plain").body(greeting)
}

async fn random_motivation(name: web::Path<String>) -> impl Responder {
    let motivations = vec![
        "Keep pushing forward, {}!",
        "You're doing great, {}!",
        "Stay positive and strong, {}!",
        "Keep up the good work, {}!",
        "Your efforts will pay off, {}!",
    ];
    let mut rng = rand::thread_rng();
    let motivation = match motivations.get(rng.gen_range(0..motivations.len())) {
        Some(motivation) => motivation.replace("{}", &name),
        None => "Motivation not found.".to_string(),
    };

    let html_response = format!(
        r#"<!DOCTYPE html>
        <html lang="en">
        <head>
            <meta charset="UTF-8">
            <title>Motivation for {}</title>
        </head>
        <body>
            <h1>Your Personalized Motivation</h1>
            <p>{}</p>
            <a href="/">Back to Home</a>
        </body>
        </html>"#,
        name, motivation
    );

    HttpResponse::Ok()
        .content_type("text/html")
        .body(html_response)
}

// Main function to setup and run the Actix Web server
#[actix_web::main]
async fn main() -> std::io::Result<()> {
    HttpServer::new(|| {
        App::new()
            .route("/random_greeting", web::get().to(random_greeting))
            .route("/motivate/{name}", web::get().to(random_motivation))
            .service(Files::new("/", "./static/root").index_file("index.html"))
    })
    .bind(("0.0.0.0", 50505))?
    .run()
    .await
}

#[cfg(test)]
mod tests {
    use actix_web::{http, test, web, App, Error};

    use super::*;

    #[actix_web::test]
    async fn test_random_greeting() -> Result<(), Error> {
        let app = App::new().route("/random_greeting", web::get().to(random_greeting));
        let app = test::init_service(app).await;

        let req = test::TestRequest::get()
            .uri("/random_greeting")
            .to_request();
        let resp = test::call_service(&app, req).await;

        assert!(resp.status().is_success());
        assert_eq!(
            resp.response()
                .headers()
                .get(http::header::CONTENT_TYPE)
                .unwrap()
                .to_str()
                .unwrap(),
            "text/plain"
        );
        Ok(())
    }

    #[actix_web::test]
    async fn test_random_motivation() -> Result<(), Error> {
        let app = App::new().route("/motivate/{name}", web::get().to(random_motivation));
        let app = test::init_service(app).await;

        let req = test::TestRequest::get().uri("/motivate/John").to_request();
        let resp = test::call_service(&app, req).await;

        assert!(resp.status().is_success());
        assert_eq!(
            resp.response()
                .headers()
                .get(http::header::CONTENT_TYPE)
                .unwrap()
                .to_str()
                .unwrap(),
            "text/html"
        );
        Ok(())
    }
}
